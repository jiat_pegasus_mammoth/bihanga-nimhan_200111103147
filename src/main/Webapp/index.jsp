<%@ page import="java.sql.Connection" %>
<%@ page import="db.DBConnection" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="com.ligeons.web.user_register" %>
<%--
  Created by IntelliJ IDEA.
  User: CHAMA COMPUTERS
  Date: 3/31/2023
  Time: 4:52 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="ISO-8859-1">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p"
            crossorigin="anonymous"></script>
    <title>Web Practical 01</title>
</head>
<body>
<div class="container-fluid">
    <%
        if (com.ligeons.web.user_register.errormsg == null) {

        } else {
    %>
    <div class="container mt-2">
        <div class="alert alert-danger" role="alert">
            <%= com.ligeons.web.user_register.errormsg %>
        </div>
    </div>
    <%
        }
    %>

    <div class="row mt-5">
        <div class="col-12 text-center">
            <h4>Customer Registreation System</h4>
        </div>
    </div>
    <div class="row mt-5">
        <div class="col-12">
            <form action="user_register" class="row g-3 p-5" method="post">
                <div class="col-md-12">
                    <label class="form-label">Name</label>
                    <input type="text" class="form-control" name="name">
                </div>
                <div class="col-12">
                    <label class="form-label">Address</label>
                    <input type="text" class="form-control" name="address">
                </div>
                <div class="col-md-8">
                    <label class="form-label">Phone</label>
                    <input type="text" class="form-control" name="phone">
                </div>
                <div class="col-md-4">
                    <label class="form-label">Gender</label>
                    <select name="gender" class="form-select">
                        <option selected>Choose...</option>
                        <option value="1">Male</option>
                        <option value="2">Female</option>
                    </select>
                </div>
                <div class="col-12">
                    <button type="submit" class="btn btn-primary">Sign Up</button>
                </div>
            </form>
        </div>
    </div>


    <%--    table here--%>
    <div class="row">
        <div class="col-12 p-5">
            <table class="table  table-hover">

                <tr>
                    <th> User ID</th>
                    <th> Name</th>
                    <th> Address</th>
                    <th> Mobile Number</th>
                    <th> Gender</th>
                    <th> Actions</th>
                </tr>

                <%

                    Connection connection = null;

                    try {

                        connection = DBConnection.getConnection();

                        ResultSet result = DBConnection.search("SELECT * FROM `customer` INNER JOIN `gender` ON `customer`.`gender_id`=`gender`.`id` ORDER BY `customer`.`id` ASC");

                        while (result.next()) {
                %>
                <tr>

                    <td><%= result.getString("id") %>
                    </td>
                    <td><%= result.getString("name") %>
                    </td>
                    <td><%= result.getString("address") %>
                    </td>
                    <td><%= result.getString("phone") %>
                    </td>
                    <td><%= result.getString("gender.name") %>
                    <td>
                        <div class="row">
                            <div class="col-3">
                                <form action="user_delete" method="POST">
                                    <input type="hidden" name="Duid" value="<%= result.getString("id") %>"/>
                                    <input type="submit" value="Delete" class="btn btn-danger">
                                </form>
                            </div>
                            <div class="col-3">
                                <form action="updateUser" method="POST">
                                    <input type="hidden" name="Uuid" value="<%= result.getString("id") %>"/>
                                    <input type="submit" value="Update" class="btn btn-info">
                                </form>
                            </div>
                        </div>
                    </td>

                </tr>
                <%
                        }

                    } catch (Exception ex) {
                        ex.printStackTrace();
                    } finally {

                        if (connection != null) {


                        }

                    }

                %>

            </table>
        </div>
    </div>
</div>
</body>
</html>
