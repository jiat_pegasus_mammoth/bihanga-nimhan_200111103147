package com.ligeons.web;

import db.DBConnection;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public class user_register extends HttpServlet {

    public static String errormsg;

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html");

        String name = req.getParameter("name");
        String address = req.getParameter("address");
        String phone = req.getParameter("phone");
        String gender = req.getParameter("gender");

        if (name.isEmpty()) {
            errormsg = "Name can't be empty !";
            resp.sendRedirect("index.jsp");

        } else if (address.isEmpty()) {
            errormsg = "Address can't be empty !";
            resp.sendRedirect("index.jsp");

        } else if (phone.isEmpty()) {
            errormsg = "Phone number can't be empty !";
            resp.sendRedirect("index.jsp");

        } else if (gender == "") {
            errormsg = "Gender can't be empty !";
            resp.sendRedirect("index.jsp");

        } else {
            errormsg = "";

            Connection connection = null;

            try {

                connection = DBConnection.getConnection();

                DBConnection.iud("INSERT INTO `customer`(`name`,`address`,`phone`,`gender_id`) VALUES ('" + name + "','" + address + "','" + phone + "','" + gender + "')");

                resp.sendRedirect("index.jsp");

            } catch (Exception ex) {
                ex.printStackTrace();
            } finally {

                if (connection != null) {


                }

            }
        }

    }

}
